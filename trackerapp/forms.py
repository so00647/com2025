from django import forms
from .models import Plan, Workout, Exercise

# Plan form creation
class PlanForm(forms.ModelForm):
    
    # Meta Class
    class Meta: 
        # Links up to the Plan model and assigns the fields to widgets
        model = Plan
        fields = ['name', 'description']
        widgets = {
            'name' : forms.TextInput(attrs={
                'class' : 'formfield',
                'placeholder' : 'Plan Name',
            }),
            'description': forms.Textarea(attrs={
                'class': 'formfield',
                'placeholder': 'Plan Description',
                'rows' : 25,
                'cols' : 60,
             })
        }

# workout form creation
class WorkoutForm(forms.ModelForm):
    
    # Meta Class
    class Meta:
        # Links up to the Workout model and assigns the fields to widgets
        model = Workout
        
        weekday = forms.ChoiceField(widget=forms.Select(choices=Workout.WEEKDAYS))
        
        fields = ['name', 'weekday', 'plan']
        
        widgets = {
            'name' : forms.TextInput(attrs={
            'class' : 'formfield',
            'placeholder' : 'Workout Name',
            }),
            'plan' : forms.HiddenInput(),                                       
        }
        
# exercise form creation
class ExerciseForm(forms.ModelForm):
        
    #Meta Class
    class Meta:
        # Links up to the Exercise model and assigns the fields to widgets
        model = Exercise
        type = forms.ChoiceField(widget=forms.Select(choices=Exercise.TYPES))
        fields = ['name', 'type', 'set_Rep_Count', 'working_Weight', 'working_Time', 'rest_Time', 'workout']
            
        widgets = {
            'name' : forms.TextInput(attrs={
            'class' : 'formfield',
            'placeholder' : 'Exercise Name',
            }),
            'set_Rep_Count' : forms.TextInput(attrs={
            'class' : 'formfield',
            'placeholder'  : 'Format: "Set Number"x"Rep Number e.g. 5x10"(Optional)',
            }),
            'working_Weight' : forms.TextInput(attrs={
            'class' : 'formfield',
            }),
            'working_Time' : forms.NumberInput(attrs={
            'class' : 'formfield',
            'placeholder'  : 'Minutes e.g. 30(Optional)',
            }),
            'rest_Time' : forms.NumberInput(attrs={
            'class' : 'formfield',
            'placeholder'  : 'Minutes e.g. 30(Optional)',
            }),
            'workout' : forms.HiddenInput()
        }
        