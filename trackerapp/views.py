from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.contrib import messages
from .forms import PlanForm, WorkoutForm, ExerciseForm
from .models import Plan, Workout, Exercise

# Plan Index View
def index_view(request):
    context = {}
    context["plan_list"] = Plan.objects.all()
    return render(request, "trackerapp/index.html", context)

# Plan Detail View
def PlanDetailView(request, pid):
    context = {}
    context["plan"] = Plan.objects.get(id=pid)
    context["workout_list"] = Workout.objects.filter(plan__id=pid)
    return render(request, "trackerapp/detail_view.html", context)

# Workout Index View
def WorkoutListView(request, pid):
    context = {}
    context["workout_list"] = Workout.objects.filter(plan__id=pid)
    return render(request, "trackerapp/workouts_index.html", context)

# Workout Detail View
def WorkoutDetailView(request, wid):
    context = {}
    context["workout"] = Workout.objects.get(id=wid)
    context["exercise_list"] = Exercise.objects.filter(workout__id=wid)
    return render(request, "trackerapp/workouts_detail_view.html", context)  

# Exercise Index View
def ExerciseListView(request, wid):
    context = {}
    context["exercise_list"] = Exercise.objects.filter(workout__id=wid)
    return render(request, "trackerapp/exercises_index.html", context)

# Exercise Detail View
def ExerciseDetailView(request, eid):
    context = {}
    context["exercise"] = Exercise.objects.get(id=eid)
    return render(request, "trackerapp/exercises_detail_view.html", context)

# Plan Create View
def create_view(request):
    context = {}
    form = PlanForm(request.POST or None)
    if(request.method == 'POST'):
        if( form.is_valid()):
            form.save()
            messages.add_message(request, messages.SUCCESS, 'New Workout Plan Created')
            return redirect('tracker_index')
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data: No Workout Plan Created')
        
    context['form'] = form
    return render(request, "trackerapp/create_view.html", context)

# Workout Create View
def CreateWorkoutsView(request, pid):
    context = {}
    form = WorkoutForm(request.POST or None)
    if(request.method == 'POST'):
        if(form.is_valid()):
            form.save()
            messages.add_message(request, messages.SUCCESS, 'New Workout Created')
            return redirect('tracker_detail', pid = pid)
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data: No Workout Created')
    
    context['form'] = form
    context['form'].fields['plan'].initial = pid
    return render(request, "trackerapp/workouts_create_view.html", context) 

# Exercise Create View
def CreateExercisesView(request, wid):
    context = {}
    form = ExerciseForm(request.POST or None)
    if(request.method == 'POST'):
        if(form.is_valid()):
            form.save()
            messages.add_message(request, messages.SUCCESS, 'New Exercise Created')
            return redirect('workouts_detail', wid = wid)
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data: No Exercise Created')
    
    context['form'] = form
    context['form'].fields['workout'].initial = wid
    return render(request, "trackerapp/exercises_create_view.html", context)

# Plan Update View
def update_view(request, pid):
    context = {}
    obj = get_object_or_404(Plan, id = pid)
    form = PlanForm(request.POST or None, instance = obj)
    
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'Workout Plan Updated')
        return redirect('tracker_detail', pid = pid)
    else:
        messages.add_message(request, messages.ERROR, 'Invalid Form Data: No Workout Plan Updated')
    
    context['form'] = form
    return render(request, "trackerapp/update_view.html", context)
  
# Workout Update View  
def UpdateWorkoutsView(request, wid):
    context = {}
    obj = get_object_or_404(Workout, id = wid)
    form = WorkoutForm(request.POST or None, instance = obj)
    if(request.method == 'POST'):
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Workout Updated')
            return redirect('workouts_detail', wid = wid)
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data: No Workout Updated')

    context['form'] = form
    return render(request, "trackerapp/workouts_update_view.html", context)   

# Exercise Update View
def UpdateExercisesView(request, eid):
    context = {}
    obj = get_object_or_404(Exercise, id = eid)
    form = ExerciseForm(request.POST or None, instance = obj)
    if(request.method == 'POST'):
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Exercise Updated')
            return redirect('exercises_detail', eid = eid)
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data: No Exercise Updated')

    context['form'] = form
    return render(request, "trackerapp/exercises_update_view.html", context)   

# Plan Delete View
def delete_view(request, pid):
    obj = get_object_or_404(Plan, id = pid)
    obj.delete()
    messages.add_message(request, messages.SUCCESS, 'Workout Plan Deleted')
    return redirect('tracker_index')

# Workout Delete View
def DeleteWorkoutsView(request, wid):
    workout = Workout.objects.get(pk=wid)
    pid = workout.plan_id
    workout.delete()
    messages.add_message(request, messages.SUCCESS, 'Workout Deleted')
    return redirect('tracker_detail', pid)

# Exercise Delete View
def DeleteExercisesView(request, eid):
    exercise = Exercise.objects.get(pk=eid)
    wid = exercise.workout_id
    exercise.delete()
    messages.add_message(request, messages.SUCCESS, 'Exercise Deleted')
    return redirect('workouts_detail', wid)
        
            