# Generated by Django 4.1.3 on 2022-12-06 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trackerapp', '0013_rename_workingweight_exercise_working_weight_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workout',
            name='weekday',
            field=models.CharField(choices=[('Monday', 'Monday'), ('Tuesday', 'Tuesday'), ('Wednesday', 'Wednesday'), ('Thursday', 'Thursday'), ('Friday', 'Friday'), ('Saturday', 'Saturday'), ('Sunday', 'Sunday')], default='Monday', max_length=9),
        ),
    ]
