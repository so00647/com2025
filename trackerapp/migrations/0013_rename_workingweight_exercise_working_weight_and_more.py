# Generated by Django 4.1.3 on 2022-12-06 15:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trackerapp', '0012_alter_exercise_resttime_alter_exercise_workingtime'),
    ]

    operations = [
        migrations.RenameField(
            model_name='exercise',
            old_name='workingWeight',
            new_name='working_Weight',
        ),
        migrations.RemoveField(
            model_name='exercise',
            name='restTime',
        ),
        migrations.RemoveField(
            model_name='exercise',
            name='set_repCount',
        ),
        migrations.RemoveField(
            model_name='exercise',
            name='workingTime',
        ),
        migrations.AddField(
            model_name='exercise',
            name='rest_Time',
            field=models.DurationField(blank=True, max_length=8, null=True),
        ),
        migrations.AddField(
            model_name='exercise',
            name='set_Rep_Count',
            field=models.CharField(blank=True, max_length=5),
        ),
        migrations.AddField(
            model_name='exercise',
            name='working_Time',
            field=models.DurationField(blank=True, max_length=8, null=True),
        ),
    ]
