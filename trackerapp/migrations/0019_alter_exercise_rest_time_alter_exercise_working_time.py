# Generated by Django 4.1.3 on 2022-12-06 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trackerapp', '0018_alter_exercise_rest_time_alter_exercise_working_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='rest_Time',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='exercise',
            name='working_Time',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
