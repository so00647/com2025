from django.urls import path
from . import views

urlpatterns = [
    # workout plan lists
    path('', views.index_view, name='tracker_index'),
    # workout plan details (shows list of workouts over the course of a week)
    path('<int:pid>', views.PlanDetailView, name='tracker_detail'),
    # create new workout plans
    path('new', views.create_view, name='tracker_new'),
    # edit workout plans
    path('<int:pid>/edit', views.update_view, name='tracker_update'),
    # delete workout plans
    path('<int:pid>/delete', views.delete_view, name='tracker_delete'),
    # view workouts list
    path('<int:pid>/workouts', views.WorkoutListView, name='workouts_index'),
    # workout details (shows list of exercises)
    path('workouts/<int:wid>', views.WorkoutDetailView, name='workouts_detail'),
    # create new workouts
    path('<int:pid>/workouts/new', views.CreateWorkoutsView, name='workouts_new'),
    # update new workouts
    path('workouts/<int:wid>/edit', views.UpdateWorkoutsView, name='workouts_update'),
    # delete workouts
    path('workouts/<int:wid>/delete', views.DeleteWorkoutsView, name='workouts_delete'),
    # view exercises list
    path('<int:wid>/exercises', views.ExerciseListView, name='exercises_index'),
    # exercise details
    path('exercises/<int:eid>', views.ExerciseDetailView, name='exercises_detail'),
    # create new exercise
    path('<int:wid>/exercises/new', views.CreateExercisesView, name='exercises_new'),
    # edit exercise
    path('exercises/<int:eid>/edit', views.UpdateExercisesView, name='exercises_update'),
    # delete exercise
    path('exercises/<int:eid>/delete', views.DeleteExercisesView, name='exercises_delete'),
]
