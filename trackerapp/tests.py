from django.shortcuts import get_object_or_404, render, redirect
from django.db.backends.sqlite3.base import IntegrityError
from django.db import transaction
from django.test import TestCase
from django.urls import reverse
from .models import Plan, Workout, Exercise
from .forms import PlanForm, WorkoutForm, ExerciseForm


class PlanTests(TestCase):
    
    # Sets up test data
    @classmethod
    def setUpTestData(cls):
        p = Plan(name = "Test Plan 1", description = "Test Description")
        p.save()
        w = Workout(name = "Test Workout 1", weekday = "Monday", plan = p)
        w.save()
        e = Exercise(name = "Test Exercise 1", type = "Mobility", workout = w)
        e.save()
        p = Plan(name = "Test Plan 2", description = "Test Description")
        p.save()
        
    # Plan model tests  
    # Tests creating a Plan object and sees if it saves properly 
    def test_create_plans(self):
        db_count = Plan.objects.all().count()
        p = Plan(name = "Test Plan 3", description = "Test Description")
        p.save()
        self.assertEqual(db_count+1, Plan.objects.all().count())
    
    # Tests creating a Plan object with the same name as another object and see if it correctly stops the object being made
    def test_duplicate_plans(self):
        db_count = Plan.objects.all().count()
        p = Plan(name = "Test Plan 1", description = "Test Description")
        #with self.assertRaises(IntegrityError):
        try:
            with transaction.atomic():
                p.save()
        except IntegrityError:
            pass
        self.assertNotEqual(db_count+1, Plan.objects.all().count())
        
    # Plan views test
    # Tests creating a Plan using the create form
    def test_post_create_plan(self):
        db_count = Plan.objects.all().count()
        data = {
            "name": "New Plan",
            "description": " New Description",
        }
        form = PlanForm(data)
        self.assertTrue(form.is_valid())
    
    # Tests creating a Plan with an empty name and seeing if the form detects its invalid  
    def test_post_create_empty_plan(self):
        db_count = Plan.objects.all().count()
        data = {
            "name": "",
            "description": " New Description",
        }
        form = PlanForm(data)
        self.assertFalse(form.is_valid())
    
    # Tests if the Plan index and detail webpages can be accessed
    def test_post_read_plan(self):
        response = self.client.get(reverse('tracker_index'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('tracker_detail', args=[1]))
        self.assertEqual(response.status_code, 200)
    
    # Tests if a Plan object can be updated using the update form
    def test_post_update_plan(self):
        data = {
            "name" : "Updated Test Plan 1",
            "description" : "Updated Test Description",
        }
        obj = get_object_or_404(Plan, id = 1)
        form = PlanForm(data, instance=obj)
        self.assertTrue(form.is_valid())
    
    # Tests if the Plan object can be deleted
    def test_post_delete_plan(self):
        response = self.client.get(reverse('tracker_delete', args=[1]))
        with self.assertRaises(Plan.DoesNotExist):
            Plan.objects.get(pk=1)
    
    # Workout model tests
    # Tests creating a Workout object and sees if it saves properly
    def test_create_workout(self):
        db_count = Workout.objects.all().count()
        w = Workout(name = "Test Workout 3", weekday = "Monday", plan = Plan.objects.get(pk=1))
        w.save()
        self.assertEqual(db_count+1, Workout.objects.all().count())
    
    # Tests creating a Workout object with the same name as another object and see if it correctly stops the object being made
    def test_duplicate_workout(self):
        db_count = Workout.objects.all().count()
        w = Workout(name = "Test Workout 1", weekday = "Monday", plan = Plan.objects.get(pk=1))
        #with self.assertRaises(IntegrityError):
        try:
            with transaction.atomic():
                w.save()
        except IntegrityError:
            pass
        self.assertNotEqual(db_count+1, Workout.objects.all().count())  
        
    # Workout view tests
    # Tests creating a Workout using the create form
    def test_post_create_workout(self):
        data = {
            "name" : "Full Body 1",
            "weekday" : "Monday",
            "plan" : Plan.objects.get(pk=1)
        }
        form = WorkoutForm(data)
        self.assertTrue(form.is_valid())
    
    # Tests creating a Workout with an empty name and seeing if the form detects its invalid  
    def test_post_create_empty_workout(self):
        data = {
            "name" : "",
            "weekday" : "Monday",
            "plan" : Plan.objects.get(pk=1)
        }
        form = WorkoutForm(data)
        self.assertFalse(form.is_valid())
    
    # Tests if the Workout index and detail webpages can be accessed
    def test_post_read_workout(self):
        response = self.client.get(reverse('workouts_index', args=[1]))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('workouts_detail', args=[1]))
        self.assertEqual(response.status_code, 200)
    
    # Tests if a Workout object can be updated using the update form
    def test_post_update_workout(self):
        data = {
            "name" : "Updated Test Workout 1",
            "weekday" : "Tuesday",
            "plan" : Plan.objects.get(pk=1)
        }
        obj = get_object_or_404(Workout, id = 1)
        form = WorkoutForm(data, instance=obj)
        self.assertTrue(form.is_valid())
    
    # Tests if the Workout object can be deleted
    def test_post_delete_workout(self):
        response = self.client.get(reverse('workouts_delete', args=[1]))
        with self.assertRaises(Workout.DoesNotExist):
            Workout.objects.get(pk=1)

    # Exercise model tests
    # Tests creating a Exercise object and sees if it saves properly
    def test_create_exercise(self):
        db_count = Exercise.objects.all().count()
        e = Exercise(name = "Test Exercise 3", type = "Mobility", workout = Workout.objects.get(pk=1))
        e.save()
        self.assertEqual(db_count+1, Exercise.objects.all().count())
    
    # Exercise view tests
    # Tests creating a Exercise using the create form
    def test_post_create_exercise(self):
        data = {
            "name" : "Test Resistance 1",
            "type" : "Resistance",
            "workout" : Workout.objects.get(pk=1)
        }
        form = ExerciseForm(data)
        self.assertTrue(form.is_valid())
    
    # Tests creating a Exercise with an empty name and seeing if the form detects its invalid
    def test_post_create_empty_exercise(self):
        data = {
            "name" : "",
            "type" : "Resistance",
            "workout" : Workout.objects.get(pk=1)
        }
        form = ExerciseForm(data)
        self.assertFalse(form.is_valid())
    
    # Tests if the Exercise index and detail webpages can be accessed   
    def test_post_read_exercise(self):
        response = self.client.get(reverse('exercises_index', args=[1]))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('exercises_detail', args=[1]))
        self.assertEqual(response.status_code, 200)
    
    # Tests if a Exercise object can be updated using the update form
    def test_post_update_exercise(self):
        data = {
            "name" : "Updated Test Resistance 1",
            "type" : "Resistance",
            "workout" : Workout.objects.get(pk=1)
        }
        obj = get_object_or_404(Exercise, id = 1)
        form = ExerciseForm(data, instance=obj)
        self.assertTrue(form.is_valid())
    
    # Tests if the Exercise object can be deleted
    def test_post_delete_exercise(self):
        response = self.client.get(reverse('exercises_delete', args=[1]))
        with self.assertRaises(Exercise.DoesNotExist):
            Exercise.objects.get(pk=1)  