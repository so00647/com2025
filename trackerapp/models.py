from django.db import models

# Plan Model
class Plan(models.Model):
    name = models.CharField(max_length = 128, 
                            unique = True, 
                            blank = False)
    description = models.TextField()
    
    # Creates an index using the name field
    class Meta:
        indexes = [models.Index(fields=['name']),]

class Workout(models.Model):
    
    # Choice Tuple for the weekday field
    WEEKDAYS = (
        ("Monday", "Monday"),
        ("Tuesday", "Tuesday"),
        ("Wednesday", "Wednesday"),
        ("Thursday", "Thursday"),
        ("Friday", "Friday"),
        ("Saturday", "Saturday"),
        ("Sunday", "Sunday"),
    )
    
    name = models.CharField(max_length = 128, 
                            unique = True, 
                            blank = False)
    weekday = models.CharField(max_length = 9, 
                               choices = WEEKDAYS,
                               default = "Monday")
    # Foreign Key linking the workout to the corresponding plan
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    
class Exercise(models.Model):
    
    # Choice Tuple for the type field
    TYPES = (
        ("Resistance", "Resistance"),
        ("Mobility", "Mobility"),
        ("Cardio", "Cardio"),
    )
    
    name = models.CharField(max_length = 128, 
                            blank = False)
    type = models.CharField(max_length = 10,
                            choices = TYPES,
                            default = "Resistance")
    set_Rep_Count = models.CharField(max_length = 5, blank=True)
    working_Weight = models.CharField(max_length = 16, blank=True)
    working_Time = models.PositiveIntegerField(blank=True, null=True)
    rest_Time = models.PositiveIntegerField(blank=True, null=True)
    # Foreign Key linking the exercise to the corresponding workout
    workout = models.ForeignKey(Workout, on_delete=models.CASCADE)
    
    
    
    
