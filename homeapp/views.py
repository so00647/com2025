from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse
from .forms import ContactForm

# View for the home page
def home(request):
    context = {}
    #Returns home page
    return render(request, 'homeapp/home.html', context)

# View for contact page
def contact(request):
    #Checks for request Method using an if statement
    if request.method == "GET":
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        # If all form fields pass validation
        if form.is_valid():
            #Validated data fields
            name = form.cleaned_data['name']
            subject = form.cleaned_data['subject']
            email = form.cleaned_data['email']
            message = name + ':\n' + form.cleaned_data['message']
            # Try catch to send contact form
            try:
                # Sends contact form data to email if connected to server
                send_mail(subject, message, email, ['so00647@surrey.ac.uk'])
            except BadHeaderError():
                # Displays message box for header errors
                messages.add_message(request, messages.ERROR, 'Message Not Sent')
                return HttpResponse("Invalid header found.")
            
            # Returns to home page and displays successful message sent box
            messages.add_message(request, messages.SUCCESS, 'Message Sent')
            return redirect(reverse('home'))
        else:
            # Else statement for when data is invalid
            messages.add_message(request, messages.ERROR, 'Invalid Form Data; Message Not Sent')
    
    # Render statement for contact page, sends the form data as input
    return render(request, 'homeapp/contact.html', {"form": form})
