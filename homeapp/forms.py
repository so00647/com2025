from django import forms

#Stores the fields to be used in the contact form, applies any validations to them , assigns a widget to them and a css class for that widget
class ContactForm(forms.Form):
    name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'formfield'}))
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'formfield'}))
    subject = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'formfield'}))
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'formfield'}), required=True)