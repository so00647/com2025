from django.urls import path
from . import views

urlpatterns = [
    # Home Page
    path('', views.home, name='home'),
    # Contact Page
    path('contact', views.contact, name='contact')
]
